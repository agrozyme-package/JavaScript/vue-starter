// import dynamicImport from 'vite-plugin-dynamic-import';
// import metaLayouts from 'vite-plugin-vue-meta-layouts';

import vueI18n from '@intlify/unplugin-vue-i18n/vite';
import vuePlugin from '@vitejs/plugin-vue';
import { resolve } from 'node:path';
import autoImport from 'unplugin-auto-import/vite';
import fonts from 'unplugin-fonts/vite';
import iconsResolver from 'unplugin-icons/resolver';
import icons from 'unplugin-icons/vite';
import { VueUseComponentsResolver, VueUseDirectiveResolver } from 'unplugin-vue-components/resolvers';
import components from 'unplugin-vue-components/vite';
import { defineConfig } from 'vite';
import pagesPlugin from 'vite-plugin-pages';

// import { nodePolyfills } from 'vite-plugin-node-polyfills';

// https://vitejs.dev/config/
// noinspection JSUnusedGlobalSymbols

export default defineConfig({
  plugins: [
    // dynamicImport(),
    // nodePolyfills({ protocolImports: true }),
    autoImport({
      dts: 'src/auto-imports.d.ts',
      resolvers: [iconsResolver()],
    }),
    components({
      dts: 'src/components.d.ts',
      resolvers: [iconsResolver(), VueUseComponentsResolver(), VueUseDirectiveResolver()],
    }),
    icons(),
    fonts(),
    // metaLayouts(),
    pagesPlugin({ routeBlockLang: 'yaml' }),
    vueI18n({ defaultSFCLang: 'yaml', include: [resolve(__dirname, './src/locales/**')] }),
    vuePlugin(),
  ],
  server: { host: true, port: 3000, strictPort: true },
  preview: { port: 3000, strictPort: true },
  optimizeDeps: { disabled: true },
  build: {
    commonjsOptions: { include: [] },
    // sourcemap: 'inline',
    // target: 'esnext'
  },
});
