import messages from '@intlify/unplugin-vue-i18n/messages';
import { set } from '@vueuse/core';
import { createI18n } from 'vue-i18n';

const localeKey = 'locale';
const fallbackLocale = 'en';

export const setLocale = (locale: string) => {
  localStorage.setItem(localeKey, locale);
  set(i18n.global.locale, locale);
};

const detectedLocale = () => {
  const locale = localStorage.getItem(localeKey) ?? fallbackLocale;
  localStorage.setItem(localeKey, locale);
  return locale;
};

export const i18n = createI18n({
  legacy: false,
  globalInjection: true,
  locale: detectedLocale(),
  fallbackLocale,
  messages,
});
