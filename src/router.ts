// import { setupLayouts } from 'virtual:meta-layouts';
import routes from 'virtual:generated-pages';
import { createRouter, createWebHistory } from 'vue-router';

export const router = createRouter({
  // routes: setupLayouts(routes),
  routes,
  history: createWebHistory(),
});
