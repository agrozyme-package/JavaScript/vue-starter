import { createApp } from 'vue';
import { i18n } from './i18n';
// @ts-ignore
import Main from './Main.vue';
import { router } from './router';

createApp(Main).use(i18n).use(router).mount('#app');
